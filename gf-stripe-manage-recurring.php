<?php
/*
 * Plugin Name: Gravity Forms Stripe Manage Recurring Add-On
 * Description: Extends the official Gravity Forms Stripe Add-On to allow recurring payments to be limited in length.
 * Version: 1.0
 * Author: dabernathy89
 * Author URI: http://www.harrismediallc.com

--------------------------------------------------------------------
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses.
*/


add_action( 'gform_loaded', array( 'GF_Stripe_Manage_Recurring_Bootstrap', 'load' ), 10 );

class GF_Stripe_Manage_Recurring_Bootstrap {

    /**
     * Register the add-on with Gravity Forms, replacing the existing Stripe Add-On
     *
     * @since       1.0
     */
    public static function load(){

        if ( ! method_exists( 'GFForms', 'include_payment_addon_framework' ) ) {
            return;
        }

        if ( ! class_exists('GFStripe') ) {
            return;
        }

        require_once( 'class-gf-stripe-manage-recurring.php' );

        GFAddOn::register( 'GFStripeManageRecurring', 'GFStripe' );

    }

}