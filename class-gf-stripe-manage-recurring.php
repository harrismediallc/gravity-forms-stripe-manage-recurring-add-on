<?php

GFForms::include_payment_addon_framework();

class GFStripeManageRecurring extends GFStripe {

    /**
     * Stores instance of the class
     *
     * @var         object
     * @since       1.0
     */
    private static $_instance = null;

    /**
     * Returns a new or previously built instance
     *
     * @since       1.0
     * @return      object
     */
    public static function get_instance() {

        if ( self::$_instance == null ) {
            self::$_instance = new GFStripeManageRecurring();
        }

        return self::$_instance;
    }

    /**
     * Add filter to read incoming Stripe webhooks
     *
     * @since       1.0
     */
    public function init() {

        parent::init();

        add_filter( 'gform_stripe_webhook', array($this, 'handle_recurring_webhook'), 10, 2 );
    }

    /**
     * Add setting for number of times recurring payment should be made
     *
     * @since       1.0
     */
    public function feed_settings_fields() {

        $default_settings = parent::feed_settings_fields();

        $recurring_times = array(
            'name'    => 'numRecurringTimes',
            'label'   => esc_html__( 'Recurring Times', 'gravityforms' ),
            'type'    => 'field_map',
            'field_map'  => array(
                array(
                    'name'     => 'numRecurringPaymentsField',
                    'label'    => __( 'Field', 'gravityforms' )
                ),
            ),
            'tooltip' => '<h6>' . esc_html__( 'Recurring Times', 'gravityforms' ) . '</h6>' . esc_html__( 'Select the field which determines the number of times the recurring payment should be made.', 'gravityforms' )
        );

        $default_settings = $this->add_field_after( 'billingCycle', $recurring_times, $default_settings);

        return $default_settings;
    }

    /**
     * Hook into the process_subscription() function to store the max number of times a subscription should be charged in the entry's meta
     *
     * @since       1.0
     */
    public function process_subscription( $authorization, $feed, $submission_data, $form, $entry ) {

        $entry = parent::process_subscription( $authorization, $feed, $submission_data, $form, $entry );

        $subscription_id = $entry['transaction_id'];

        $max_subscription_payments_field_id = $feed['meta']['numRecurringTimes_numRecurringPaymentsField'];

        $max_subscription_payments = ($max_subscription_payments_field_id === "") ? 0 : (int)$entry[$max_subscription_payments_field_id];

        gform_update_meta( $entry['id'], "gfsmr_{$subscription_id}_max_payments", $max_subscription_payments );

        return $entry;
    }

    /**
     * Handle the incoming webhooks and cancel the subscription if the max number of charges has been reached
     *
     * @since       1.0
     */
    public function handle_recurring_webhook($action, $event) {
        $type = rgar( $event, 'type' );

        if ($type === "invoice.payment_succeeded" || $type === "invoice.payment_failed") {
            $subscription = $this->get_subscription_line_item( $event );
            if ( ! $subscription ) {
                return $action;
            }

            $subscription_id = rgar( $subscription, 'id' );
            $entry_id = $this->get_entry_by_transaction_id( $action['subscription_id'] );
            if ( ! $entry_id ) {
                return $action;
            }

            $entry = GFAPI::get_entry( $entry_id );

            $num_previous_subscription_payments = (int)gform_get_meta( $entry_id, "gfsmr_{$subscription_id}_times" );
            $max_subscription_payments = (int)gform_get_meta( $entry_id, "gfsmr_{$subscription_id}_max_payments" );

            if ($type === "invoice.payment_succeeded") {
                gform_update_meta($entry_id, "gfsmr_{$subscription_id}_times", $num_previous_subscription_payments + 1);
            }

            if ($max_subscription_payments > 0 && $num_previous_subscription_payments + 1 >= $max_subscription_payments) {

                $this->cancel( $entry );

                $action['note'] .= ' Maximum number of subscription payments (' . $max_subscription_payments . ') reached';
            }
        }

        return $action;
    }

}